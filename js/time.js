/**
 * Created by Stas on 21.10.2016.
 */

var timeText = PL302.GUI.createButton({x: 1510, y: 50, text: '', width: 250, height: 80, 'font-size': 60, 'font-color': '#08EFF6', 'font-family': 'DS_Dots'});
setInterval(function(){
    var date = new Date();
    timeText.setState({text: date.getHours() + ':' + ( (date.getMinutes()<10?'0':'') + date.getMinutes()) + ':' + ((date.getSeconds()<10?'0':'') + date.getSeconds())});
}, 1000);
