var tags = {
    All_E_Remote_Mode: function(){
        return Math.random() * 64;
    },
    Gas_Temperature: function(){
        return Math.random() * 100;
    },
    Gas_Pressure: function(){
        return Math.random() * 0.5;
    },
    Ambient_Temperature: function(){
        return Math.random() * 100;
    },
    Battery_DC_Voltage: function(){
        return Math.random() * 70;
    },
    Load_DC_Current: function(){
        return Math.random() * 100;
    },
    Battery_DC_Current: function(){
        return Math.random() * 100 - 50;
    },
    Load_DC_Power: function(){
        return Math.random() * 6000;
    },
    Gas_Flow: function(){
        return Math.random() * 5;
    },
    All_E_Power_SP: function(){
        return Math.random() * 6000;
    },
    All_E_Power: function(){
        return Math.random() * 6000;
    },
    Battery_Power: function() {
        return Math.random() * 6000 - 3000;
    },
    Auxiliary_Power: function() {
        return Math.random() * 6000;
    },
    I_TotBatCur: function() {
        return Math.random() * 10 - 5;
    },
    I_BatChrgVtg: function() {
        return Math.random() * 70;
    },
    I_BatSOC: function() {
        return Math.random() * 100;
    },
    I_BatTmp: function() {
        return Math.random() * 50;
    },
    GSS_RM: function() {
        return Math.random() * 60000;
    },
    Bat_V_Lim_LL: function() {
        return 40 + Math.random() * 20;
    },
    Bat_V_Lim_L: function() {
        return 40 + Math.random() * 20;
    },
    Bat_V_Lim_H: function() {
        return 40 + Math.random() * 20;
    },
    Bat_V_Lim_HH: function() {
        return 40 + Math.random() * 20;
    },
    Bat_V_HY_On1: function() {
        return 40 + Math.random() * 20;
    },
    Bat_V_HY_Off1: function() {
        return 40 + Math.random() * 20;
    },
    Bat_V_HY_Off1_D: function() {
        return Math.random() * 5;
    },
    Aux_Power_SP1: function() {
        return Math.random() * 5000;
    },
    Aux_Power_SP2: function() {
        return Math.random() * 5000;
    },
    Gas_P_LL: function() {
        return Math.random() * 10;
    },
    Gas_P_L: function() {
        return Math.random() * 10;
    },
    Gas_P_H: function() {
        return Math.random() * 10;
    },
    Gas_P_HH: function() {
        return Math.random() * 10;
    },
    E_Cool_FR_Min: function() {
        return Math.random() * 50;
    },
    E_Cool_IT_BS_On: function() {
        return Math.random() * 150 - 10;
    },
    E_Cool_OT_BS_On: function() {
        return Math.random() * 150 - 10;
    },
    E_Cool_IT_BS_Off: function() {
        return Math.random() * 150 - 10;
    },
    E_Cool_IT_Max: function() {
        return Math.random() * 150 - 10;
    },
    E_Cool_OT_Max: function() {
        return Math.random() * 150 - 10;
    },
    Bat_SOC_EStart: function() {
        return Math.random() * 100;
    },
    Bat_SOC_Max: function() {
        return Math.random() * 100;
    },
    P0: function() {
        return Math.random() * 1000;
    },
    K0: function() {
        return Math.random();
    },
    P1: function() {
        return Math.random() * 1000;
    },
    D_Max: function() {
        return Math.random() * 10;
    },
    E_Time_Start1: function() {
        return Math.random() * 999;
    },
    E_Time_Start2: function() {
        return Math.random() * 999;
    },
    E_Time_HY1_On: function() {
        return Math.random() * 999;
    },
    E_Time_HY2_On: function() {
        return Math.random() * 999;
    },
    E_Time_HY1_Off: function() {
        return Math.random() * 999;
    },
    E_Time_HY2_Off: function() {
        return Math.random() * 999;
    },
    E_Time_Auto_Reset: function() {
        return Math.random() * 999;
    },
    E_Reset_Max_Attempts: function() {
        return Math.random() * 100;
    },
    GSS_WH_Add: function() {
        return Math.random() * 32000;
    },
    E_WH_Add: function() {
        return Math.random() * 32000;
    },
    E1_ECU_Address: function(){
        return Math.random() * 100;
    },
    E1_ECU_Software_Version: function(){
        return Math.random() * 9;
    },
    E1_ECU_Software_Revision: function(){
        return Math.random() * 99;
    },
    E1_ECU_Software_Build: function(){
        return Math.random() * 999;
    },
    E1_VI_Prot_Software_Version: function(){
        return Math.random() * 9;
    },
    E1_VI_Prot_Software_Revision: function(){
        return Math.random() * 99;
    },
    E1_VI_Prot_Software_Build: function(){
        return Math.random() * 999;
    },
    E1_Status_Flags: function(){
        return Math.random() * 9;
    },
    E1_E_Head_Temperature_Set_Point: function(){
        return Math.random() * 600;
    },
    E1_E_Control_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E1_E_Limit_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E1_Coolant_Inlet_Temperature: function(){
        return Math.random() * 100;
    },
    E1_Coolant_Outlet_Temperature: function(){
        return Math.random() * 100;
    },
    E1_Ambient_Temperature: function(){
        return Math.random() * 100;
    },
    E1_Fan_Speed: function(){
        return Math.random() * 6000;
    },
    E1_Coolant_Flow_Rate: function(){
        return Math.random() * 50;
    },
    E1_Engine_Voltage: function(){
        return Math.random() * 1000;
    },
    E1_Engine_Current: function(){
        return Math.random() * 10;
    },
    E1_Engine_Power: function(){
        return Math.random() * 1100;
    },
    E1_Frequency: function(){
        return 49.5 + Math.random();
    },
    E1_Active_Error_Code: function(){
        return Math.random() * 1000;
    },
    E1_Engine_Run_Hours: function(){
        return Math.random() * 9999;
    },
    E1_Engine_Starts: function(){
        return Math.random() * 9999;
    },
    E1_Engine_Emergency_Stops: function(){
        return Math.random() * 9999;
    },
    E2_ECU_Address: function(){
        return Math.random() * 6;
    },
    E2_ECU_Software_Version: function(){
        return Math.random() * 99;
    },
    E2_VI_Prot_Software_Version: function(){
        return Math.random() * 99;
    },
    E2_Status_Flags: function(){
        return Math.random() * 9;
    },
    E2_E_Head_Temperature_Set_Point: function(){
        return Math.random() * 600;
    },
    E2_E_Control_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E2_E_Limit_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E2_Coolant_Inlet_Temperature: function(){
        return Math.random() * 100;
    },
    E2_Coolant_Outlet_Temperature: function(){
        return Math.random() * 100;
    },
    E2_Ambient_Temperature: function(){
        return Math.random() * 100;
    },
    E2_Fan_Speed: function(){
        return Math.random() * 6000;
    },
    E2_Coolant_Flow_Rate: function(){
        return Math.random() * 50;
    },
    E2_Engine_Voltage: function(){
        return Math.random() * 1000;
    },
    E2_Engine_Current: function(){
        return Math.random() * 10;
    },
    E2_Engine_Power: function(){
        return Math.random() * 1100;
    },
    E2_Frequency: function(){
        return 49.5 + Math.random();
    },
    E2_Active_Error_Code: function(){
        return Math.random() * 1000;
    },
    E2_Engine_Run_Hours: function(){
        return Math.random() * 9999;
    },
    E2_Engine_Starts: function(){
        return Math.random() * 9999;
    },
    E2_Engine_Emergency_Stops: function(){
        return Math.random() * 9999;
    },
    E3_ECU_Address: function(){
        return Math.random() * 6;
    },
    E3_ECU_Software_Version: function(){
        return Math.random() * 99;
    },
    E3_VI_Prot_Software_Version: function(){
        return Math.random() * 99;
    },
    E3_Status_Flags: function(){
        return Math.random() * 9;
    },
    E3_E_Head_Temperature_Set_Point: function(){
        return Math.random() * 600;
    },
    E3_E_Control_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E3_E_Limit_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E3_Coolant_Inlet_Temperature: function(){
        return Math.random() * 100;
    },
    E3_Coolant_Outlet_Temperature: function(){
        return Math.random() * 100;
    },
    E3_Ambient_Temperature: function(){
        return Math.random() * 100;
    },
    E3_Fan_Speed: function(){
        return Math.random() * 6000;
    },
    E3_Coolant_Flow_Rate: function(){
        return Math.random() * 50;
    },
    E3_Engine_Voltage: function(){
        return Math.random() * 1000;
    },
    E3_Engine_Current: function(){
        return Math.random() * 10;
    },
    E3_Engine_Power: function(){
        return Math.random() * 1100;
    },
    E3_Frequency: function(){
        return 49.5 + Math.random();
    },
    E3_Active_Error_Code: function(){
        return Math.random() * 1000;
    },
    E3_Engine_Run_Hours: function(){
        return Math.random() * 9999;
    },
    E3_Engine_Starts: function(){
        return Math.random() * 9999;
    },
    E3_Engine_Emergency_Stops: function(){
        return Math.random() * 9999;
    },
    E4_ECU_Address: function(){
        return Math.random() * 6;
    },
    E4_ECU_Software_Version: function(){
        return Math.random() * 99;
    },
    E4_VI_Prot_Software_Version: function(){
        return Math.random() * 99;
    },
    E4_Status_Flags: function(){
        return Math.random() * 9;
    },
    E4_E_Head_Temperature_Set_Point: function(){
        return Math.random() * 600;
    },
    E4_E_Control_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E4_E_Limit_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E4_Coolant_Inlet_Temperature: function(){
        return Math.random() * 100;
    },
    E4_Coolant_Outlet_Temperature: function(){
        return Math.random() * 100;
    },
    E4_Ambient_Temperature: function(){
        return Math.random() * 100;
    },
    E4_Fan_Speed: function(){
        return Math.random() * 6000;
    },
    E4_Coolant_Flow_Rate: function(){
        return Math.random() * 50;
    },
    E4_Engine_Voltage: function(){
        return Math.random() * 1000;
    },
    E4_Engine_Current: function(){
        return Math.random() * 10;
    },
    E4_Engine_Power: function(){
        return Math.random() * 1100;
    },
    E4_Frequency: function(){
        return 49.5 + Math.random();
    },
    E4_Active_Error_Code: function(){
        return Math.random() * 1000;
    },
    E4_Engine_Run_Hours: function(){
        return Math.random() * 9999;
    },
    E4_Engine_Starts: function(){
        return Math.random() * 9999;
    },
    E4_Engine_Emergency_Stops: function(){
        return Math.random() * 9999;
    },
    E5_ECU_Address: function(){
        return Math.random() * 6;
    },
    E5_ECU_Software_Version: function(){
        return Math.random() * 99;
    },
    E5_VI_Prot_Software_Version: function(){
        return Math.random() * 99;
    },
    E5_Status_Flags: function(){
        return Math.random() * 9;
    },
    E5_E_Head_Temperature_Set_Point: function(){
        return Math.random() * 600;
    },
    E5_E_Control_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E5_E_Limit_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E5_Coolant_Inlet_Temperature: function(){
        return Math.random() * 100;
    },
    E5_Coolant_Outlet_Temperature: function(){
        return Math.random() * 100;
    },
    E5_Ambient_Temperature: function(){
        return Math.random() * 100;
    },
    E5_Fan_Speed: function(){
        return Math.random() * 6000;
    },
    E5_Coolant_Flow_Rate: function(){
        return Math.random() * 50;
    },
    E5_Engine_Voltage: function(){
        return Math.random() * 1000;
    },
    E5_Engine_Current: function(){
        return Math.random() * 10;
    },
    E5_Engine_Power: function(){
        return Math.random() * 1100;
    },
    E5_Frequency: function(){
        return 49.5 + Math.random();
    },
    E5_Active_Error_Code: function(){
        return Math.random() * 1000;
    },
    E5_Engine_Run_Hours: function(){
        return Math.random() * 9999;
    },
    E5_Engine_Starts: function(){
        return Math.random() * 9999;
    },
    E5_Engine_Emergency_Stops: function(){
        return Math.random() * 9999;
    },
    E6_ECU_Address: function(){
        return Math.random() * 6;
    },
    E6_ECU_Software_Version: function(){
        return Math.random() * 99;
    },
    E6_VI_Prot_Software_Version: function(){
        return Math.random() * 99;
    },
    E6_Status_Flags: function(){
        return Math.random() * 9;
    },
    E6_E_Head_Temperature_Set_Point: function(){
        return Math.random() * 600;
    },
    E6_E_Control_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E6_E_Limit_Head_Temperature: function(){
        return Math.random() * 600;
    },
    E6_Coolant_Inlet_Temperature: function(){
        return Math.random() * 100;
    },
    E6_Coolant_Outlet_Temperature: function(){
        return Math.random() * 100;
    },
    E6_Ambient_Temperature: function(){
        return Math.random() * 100;
    },
    E6_Fan_Speed: function(){
        return Math.random() * 6000;
    },
    E6_Coolant_Flow_Rate: function(){
        return Math.random() * 50;
    },
    E6_Engine_Voltage: function(){
        return Math.random() * 1000;
    },
    E6_Engine_Current: function(){
        return Math.random() * 10;
    },
    E6_Engine_Power: function(){
        return Math.random() * 1100;
    },
    E6_Frequency: function(){
        return 49.5 + Math.random();
    },
    E6_Active_Error_Code: function(){
        return Math.random() * 1000;
    },
    E6_Engine_Run_Hours: function(){
        return Math.random() * 9999;
    },
    E6_Engine_Starts: function(){
        return Math.random() * 9999;
    },
    E6_Engine_Emergency_Stops: function(){
        return Math.random() * 9999;
    }
};


$.ajax = function (param) {
    if(param.data.read){
        var reply = {};
        reply[param.data.read] = tags[param.data.read]();
        param.success(reply);
    }
};
